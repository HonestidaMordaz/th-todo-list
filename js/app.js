var taskInput = document.getElementById('new-task');
var addButton = document.getElementByTagName('button')[0];
var incompleteTaskHolder = document.getElementById('incomplete-tasks');
var completedTasksHolder = document.getElementById('completed-tasks');

addButton.addEventListener('click', addTask);

for (var index = 0, len = completedTasksHolder.children.length; index < len; ++index) {
	bindTaskEvents(incompleteTaskHolder.children[index], taskCompleted);
}

for (var index = 0, len = completedTaskHolder.children.length; index < len; ++index) {
	bindTaskEvents(completedTasksHolder.children[index], taskIncmplete);
}

function addTask () {
	if (taskInput.value !== '') {
		var listItem = document.createElement('li');
		var checkbox = document.createElement('input');
		var label = document.createElement('label');
		var editInput = document.createElement('input');
		var editButton = document.createElement('input');
		var deleteButton = document.createElement('input');
		
		checkbox.type = 'checkbox';
		editInput.type = 'text';
		
		label.innerText = taskInput.value;
		editButton.innerText = 'Edit';
		deleteButton.innerText = 'Delete';
		
		editButton.className = 'edit';
		deleteButton.className = 'delete';
		
		listItem.appendChild(checkbox);
		listItem.appendChild(label);
		listItem.appendChild(editInput);
		listItem.appendChild(editButton);
		listItem.appendChild(deleteButton);
		
		incompleteTaskHolder.appendChild(listItem);
		bindTaskEvents(listItem, taskCompleted);
		
		taskInput.value = '';
	} else {
		taskInput.placeholder = 'Enter a task';
		taskInput.focus();
	}
}

function editTask () {
	var listItem = this.parentNode;
	var editInput = listItem.querySelector('input[type=text]');
	var editButton = listItem.querySelector('button.edit');
	var label = listItem.querySelector('label');
	
	if (listItem.classList.contains('editMode')) {
		label.innerText = editInput.value;
		editButton.innerText = 'Edit';
	} else {
		editInput.value = label.innerText;
		editButton.innerText = 'Save';
	}
	
	listItem.classList.toggle('editMode');
}

function deleteTask () {
	var listItem = this.parentNode;
	var ul = listItem.parentNode;
	
	ul.removeChild(listItem);
}

function taskCompleted () {
	var listItem = this.parentNode;
	completedTasksHolder.appendChild(listItem);
	bindTaskEvents(listItem, taskIncomplete);
}

function taskIncomplete () {
	var listItem = this.parentNode;
	incompleteTaskHolder.appendChild(listItem);
	bindTaskEvents(listItem, taskCompleted);
}

function bindTaskEvents (taskListItem, checkboxEventHandler) {
	var checkbox = taskListItem.querySelector('input[type=checkbox]');
	checkbox.onchange = checkboxEventHandler;
	
	var editButton = taskListItem.querySelector('button.edit');
	editButton.addEventListener('click', editTask);
	
	var deleteButton = taskListItem.querySelector('button.delete');
	deleteButton.addEventListener('click', deleteTask);
}